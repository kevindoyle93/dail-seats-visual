class Row {
    
    Seat[] seats;
    
    Row(int capacity) {
        seats = new Seat[capacity];
    }
    
    void display(float radius, float centX, float centY, float seatSize) {
        
        float thetaSpace = PI / (seats.length - 1);
        float theta = PI / 2 * 3;;
      
        for(Seat seat : seats) {
            float x, y;
        
            x = centX + sin(theta) * radius;
            y = centY + cos(theta) * radius;
        
            seat.display(x, y, seatSize);
            theta -= thetaSpace;
      
        }
        
    }
    
}