PGraphics pg;

Dail dail;

JSONArray dailArray;

void setup() {
    
    dailArray = loadJSONArray("dail_members.json");
    
    size(1000, 667);
    smooth();
    
    pg = createGraphics(width, height);
}

Boolean finished = false;
int count = 0;
void draw() {
    
    //pg.clear();
    pg.beginDraw();
    pg.background(0, 0);
    
    newDail(dailArray.getJSONObject(count));
    dail.display();
    pg.save("dail-" + dail.dailNum + ".png");
    println("dail-" + dail.dailNum + ".png saved");
    
    pg.endDraw();
    
    if(++count > dailArray.size() - 1)
        exit();
}

void newDail(JSONObject json) {
    
    int dailNum = json.getInt("dailNumber");
    String startDate = json.getString("startDate");
    String endDate = json.getString("endDate");
    JSONArray parties = json.getJSONArray("parties");
    
    Party[] dailParties = new Party[parties.size()];
    
    for(int i = 0; i < parties.size(); i++) {
        JSONObject party = parties.getJSONObject(i);
        String partyName = party.getString("name");
        String partyInitials = party.getString("initials");
        int TDs = party.getInt("TDs");
        color colour = unhex("FF" + party.getString("colour").substring(1));
        
        dailParties[i] = new Party(partyName, partyInitials, colour, TDs);
    }
    
    int year = Integer.parseInt(startDate.substring(startDate.lastIndexOf(' ') + 1));
    
    dail = new Dail(year, dailParties, startDate, endDate, dailNum);
}