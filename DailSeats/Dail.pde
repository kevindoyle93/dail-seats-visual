class Dail {
  
    int year;
    String startDate;
    String endDate;
    int dailNum;
    String title;
    Row[] rows;
    Seat[] seats;
    Party[] parties;
    
    final int[] ROW_CAPACITIES = new int[] {15, 18, 21, 24, 26, 29, 33};
    
    Dail(int dailNum) {
        this.dailNum = dailNum;
        
        seats = new Seat[166];
        for(int i = 0; i < seats.length; i++) {
            seats[i] = new Seat();
        }
        
        rows = new Row[7];
        for(int i = 0; i < rows.length; i++)
            rows[i] = new Row(ROW_CAPACITIES[i]);
            
        
        if(dailNum % 10 == 1)
            title = "The " + dailNum + "st Dáil";
        else if(dailNum % 10 == 2)
            title = "The " + dailNum + "nd Dáil";
        else if(dailNum % 10 == 3)
            title = "The " + dailNum + "rd Dáil";
        else
            title = "The " + dailNum + "th Dáil";
    }
    
    Dail(int year, Party[] parties, String start, String end, int dailNum) {
        this(dailNum);
        
        this.year = year;
        startDate = start;
        endDate = end;
        this.parties = parties;
        
        assignSeats();
    }
    
    void assignSeats() {
        
        setSeatNumbers();
        
        int offset = 0;
        for(int i = 0; i < parties.length; i++) {
            
            for(int j = 0; j < parties[i].TDs; j++)
                seats[j + offset].party = parties[i];
            
            // This is a quick fix because the data online is missing some TDs
            // For the purposes of the diagram I'm just assigning these seats to Independents
            if(year == 2011 && parties[i].name.equals("Independents")) {
                seats[offset++ + parties[i].TDs].party = parties[i];
                seats[offset++ + parties[i].TDs].party = parties[i];
            }
            
            offset += parties[i].TDs;
        }
        
    }
    
    void display() {
        
        showParties();
        showDail();
    }
    
    private void showParties() {
        
        float w = width / 12;
        float h = w / 3 * 5;
        float gap = w / 4;
        pg.rectMode(CENTER);
        
        pg.textAlign(CENTER, CENTER);
        pg.textSize(h / 4);
        
        float x = (width / 2) - (w * parties.length / 2 + gap * parties.length / 2);
        float y = h / 2;
        
        // Centre the party seat count blocks
        x += w / 2 + gap / 2;
        
        for(int i = 0; i < parties.length; i++) {
            pg.stroke(parties[i].colour);
            pg.fill(parties[i].colour);
            pg.rect(x, y, w, h);
        
            pg.stroke(255);
            pg.fill(255);
            
            // Make sure the text fits in the box
            if(parties[i].initial.length() > 3)
                pg.textSize(h / 5);
            else
                pg.textSize(h / 4);
            
            pg.text(parties[i].initial, x, y - h / 5);
            pg.text(parties[i].TDs, x, y + h / 5);
        
            x += w + gap;
        }
    }
    
    private void showDail() {
      
        float radius = width / 4.5f;
        float seatSize = radius / 8;
        float centX = width / 2;
        float centY = height - height / 20;
        
        pg.fill(0);
        pg.textAlign(CENTER);
        pg.textSize(radius / 4);
        pg.text("" + year, centX, centY);
        
        for(int i = 0; i < rows.length; i++) {
            rows[i].display(radius, centX, centY, seatSize);
            radius += seatSize * 1.33;
        }
      
    }
    
    void setSeatNumbers() {
        
        /*
            There's probably a much nicer, more algorithmic way to do this...
            
            This method assigns the seats in the rows in such a way that the parties
            are group together like a pie chart, filling the rows going backwards
            rather than filling one row, then working backwards.
        */
        
        int sections = rows[0].seats.length;
        int[][] rowSections = new int[sections / 2 + 1][rows.length];
        
        for(int i = 0; i < rowSections.length; i++)
            rowSections[i][0] = 1;
            
        /*
            rowSections[section][row]
            
            Each section (1 - 15) has a certain number of seats, one in the front row and then usually more
            going backwards through the rows. The sections are symmetrical though, so I only need to set 
            sections 1 to 8. This part assigns a certain number of seats to each row in each section.
        */
        
        rowSections[0][1] = 2;
        rowSections[0][2] = 2;
        rowSections[0][3] = 2;
        rowSections[0][4] = 2;
        rowSections[0][5] = 2;
        rowSections[0][6] = 3;
        
        rowSections[1][1] = 1;
        rowSections[1][2] = 1;
        rowSections[1][3] = 2;
        rowSections[1][4] = 2;
        rowSections[1][5] = 2;
        rowSections[1][6] = 2;
        
        rowSections[2][1] = 1;
        rowSections[2][2] = 1;
        rowSections[2][3] = 1;
        rowSections[2][4] = 1;
        rowSections[2][5] = 2;
        rowSections[2][6] = 2;
        
        rowSections[3][1] = 1;
        rowSections[3][2] = 2;
        rowSections[3][3] = 2;
        rowSections[3][4] = 2;
        rowSections[3][5] = 2;
        rowSections[3][6] = 2;
        
        rowSections[4][1] = 1;
        rowSections[4][2] = 1;
        rowSections[4][3] = 1;
        rowSections[4][4] = 2;
        rowSections[4][5] = 2;
        rowSections[4][6] = 2;
        
        rowSections[5][1] = 1;
        rowSections[5][2] = 1;
        rowSections[5][3] = 2;
        rowSections[5][4] = 1;
        rowSections[5][5] = 2;
        rowSections[5][6] = 2;
        
        rowSections[6][1] = 2;
        rowSections[6][2] = 2;
        rowSections[6][3] = 1;
        rowSections[6][4] = 2;
        rowSections[6][5] = 2;
        rowSections[6][6] = 3;
        
        rowSections[7][1] = 0;
        rowSections[7][2] = 1;
        rowSections[7][3] = 2;
        rowSections[7][4] = 2;
        rowSections[7][5] = 1;
        rowSections[7][6] = 1;
        
        int[] offset = new int[] {0, 0, 0, 0, 0, 0, 0};
        
        int count = 0;
        
        for(int i = 0; i < sections; i++) {

            for(int j = 0; j < rows.length; j++) {
                
                // Quick fix cause I can't think right now
                Boolean quickFix = false;
                int temp = 0;
                if(i > 7) {
                    temp = i;
                    i = 7 - (i - 7);
                    quickFix = true;
                }
                
                for(int k = 0; k < rowSections[i][j]; k++)
                    rows[j].seats[k + offset[j]] = seats[count++];
                
                offset[j] += rowSections[i][j];
                
                // Quick fix cause I can't think right now
                if(quickFix)
                    i = temp;
                
            }
        }
    }
     
}