class Party {

    String name;
    String initial;
    color colour;
    int TDs;
    
    Party() {
        name = "";
        initial = "";
        colour = color(127);
        TDs = 166;
    }
    
    Party(String name, String initial, color colour, int TDs) {
        this.name = name;
        this.initial = initial;
        this.colour = colour;
        this.TDs = TDs;
    }

}