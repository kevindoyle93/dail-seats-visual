class Seat {
    
    color colour;
    int seatNumber;
    PVector location;
    Party party;
    
    Seat() {
        location = new PVector(width / 2, height / 2);
    }
    
    Seat(int seatNumber) {
        this.seatNumber = seatNumber;
        location = new PVector(width / 2, height / 2);
    }
  
    void display(float x, float y, float size) {
        
        if(party != null) {
            pg.stroke(party.colour);
            pg.fill(party.colour);
        }
        else {
            pg.stroke(127);
            pg.fill(127);
        }

        location.x = x;
        location.y = y;
        pg.ellipse(x, y, size, size);
    }
  
}